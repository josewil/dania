<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="style/estilo.css">  

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
   
         <nav class="navegador">
             <center><ul id="inicio0" class="menu">
                 <li><a href="">Inicio</a></li>      
                 <li><a id="" href="">Punto Fucion Ajustado y no Ajustado</a></li>   
                 <li><a href="">KALOC</a></li>
                 <li><a href="">Calculo COCOMO</a>
                <ul class="submenu">
                    <li><a href="">Basico</a></li>             
                    <a  href="#" id="intermedio0"> Intermedio</a> 
                    <li><a href="">Avanzado</a></li>  
                </ul>
                
         </nav>
    
 
   <script>
  function multiplicar() {
    var valor0= document.getElementById("1").value*3;
    var valor1= document.getElementById("2").value*4;
    var valor2= document.getElementById("3").value*6;
    var valor3= document.getElementById("4").value=valor0+valor1+valor2;
    var valor4= document.getElementById("5").value*4;
    var valor5= document.getElementById("6").value*5;
    var valor6= document.getElementById("7").value*7;
    var valor7= document.getElementById("8").value=valor4+valor5+valor6;
    var valor8= document.getElementById("9").value*3;
    var valor9= document.getElementById("10").value*4;
    var valor10= document.getElementById("11").value*6;
    var valor11= document.getElementById("12").value=valor8+valor9+valor10;
    var valor12= document.getElementById("13").value*7;
    var valor13= document.getElementById("14").value*10;
    var valor14= document.getElementById("15").value*15;
    var valor15= document.getElementById("16").value=valor12+valor13+valor14;
    var valor16= document.getElementById("17").value*5;
    var valor17= document.getElementById("18").value*7;
    var valor18= document.getElementById("19").value*10;
    var valor19= document.getElementById("20").value=valor16+valor17+valor18;
    var valor20= document.getElementById("21").value=valor3+valor7+valor11+valor15+valor19;
    var valor21= document.getElementById("pfsa").value=valor20;
    var valor22= document.getElementById("suma").value; 
    var valor23= document.getElementById("pfa").value=valor21*(valor22*0.01+0.65); 
    var valor24= document.getElementById("multiplicar").value=valor23;
    var valor25= document.getElementById("codigo").value; 
    var valor26= document.getElementById("kloc").value=(valor24*valor25)/1000;
    var valor27= document.getElementById("operacion").value=valor26;
    var valor27= document.getElementById("loc").value=valor26*1000; 
    if (valor26<50 && valor26>0) {
       var a=2.4;
       var b=1.05;
       var c=2.5;
       var d=0.38;
    }
    else if (valor26>=50 && valor26<300) {
         a=3.0;
         b=1.12;
         c=2.5;
         d=0.35;
    }
    else if (valor26>=300) {
         a=3.6;
         b=1.2;
         c=2.5;
         d=0.32;
    }
    
    var esfuerzo=a*Math.pow(valor26, b);
    document.getElementById("esfuerzo").value=esfuerzo;
    var tiempo=c*Math.pow(esfuerzo, d);
    document.getElementById("tiempo").value=tiempo;
    var productividad=valor27/esfuerzo;
    document.getElementById("productividad").value=productividad;
    var personal=esfuerzo/tiempo;
    document.getElementById("personal").value=personal;
  }
  function intermedio() {
     var ids='',valor=1;
           $('input[type=checkbox]:checked').each(function(){
            var $this = $(this);
            if($this.is(":checked")){
                
                var g = $this.attr("value");
                valor=valor*g;

                
                 ids += g +',';
            }
       
        });
       
        document.getElementById("fa").value=valor;
        var k=parseFloat(document.getElementById("kloc").value);
        if (k<50 && k>0) {
       var ai=3.2;
       var bi=1.05;
       var ci=2.5;
       var di=0.38;
    }
    else if (k>=50 && k<300) {
         ai=3.0;
         bi=1.12;
         ci=2.5;
         di=0.35;
    }
    else if (k>=300) {
         ai=2.8;
         bi=1.20;
         ci=2.5;
         di=0.32;
    }
    
    var esfuerzoi=ai*Math.pow(k, bi)*valor;
    document.getElementById("e").value=esfuerzoi;
    var tiempoi=ci*Math.pow(esfuerzoi, di);
    document.getElementById("t").value=tiempoi;
    var productividadi=(k*1000)/esfuerzoi;
    document.getElementById("pro").value=productividadi;
    var personali=esfuerzoi/tiempoi;
    document.getElementById("per").value=personali;
    
  }
  $(document).ready(function(){

var intermedio1=$('#intermedio1').offset().top;


$('#intermedio0').on('click',function(e){
	e.preventDefault();
	$('html,body').animate({
		scrollTop:intermedio1 -100

	},500);
});
});


   </script>
</head>
<body <textarea style='font-size: 30px; font-weight: bold'></textarea> 
<center><table  BORDER=5 CELLPADDING=10 CELLSPACING=10 bordercolor="white" <textarea style='font-size: 30px; font-weight: bold'></textarea>

<tr>
<td rowspan="2">Parametros de Medición</td>
    <td colspan="2" align="center">Simple</td>
    <td colspan="2" align="center">Media</td>
    <td colspan="2" align="center">Completa</td>
    <td rowspan="2" align="center">Total</td>
</tr>
<tr>
     <td>Cantidad</td>
     <td>Peso</td>
     <td>Cantidad</td>
     <td>Peso</td>
     <td>Cantidad</td>
     <td>Peso</td> 
</tr>
<tr>
<td align="center" >Entradas</td>
     <td nowrap>
     <input type="int" size="2" name="entrada" value="" id="1" onchange="multiplicar()">
     </td>
     <td>*3</td>
     <td>
     <input type="int" size="2" name="entrada" value="" id="2" onchange="multiplicar()">
     </td>
     <td>*4</td>
     <td>
     <input type="int" size="2" name="entrada" value="" id="3" onchange="multiplicar()">
     </td>
     <td>*6</td>
     <td>
     <input type="int" size="2" name="entrada" value="" id="4">
     </td>
</tr>
<tr>
     <td align="center">Salidas</td>
     <td>
     <input type="int" size="2" name="entrada" value="" id="5" onchange="multiplicar()">
     </td>
     <td>*4</td>
     <td>
     <input type="int" size="2" name="entrada" value="" id="6" onchange="multiplicar()">
     </td>
     <td>*5</td>
     <td>   
     <input type="int" size="2" name="entrada" value="" id="7" onchange="multiplicar()">
     </td>
     <td>*7</td>
     <td>    
     <input type="int" size="2" name="entrada" value="" id="8">
     </td>
</tr>
<tr>
     <td align="center">Consultas</td>
     <td>   
     <input type="int" size="2" name="entrada" value="" id="9" onchange="multiplicar()">
     </td>
     <td>*3</td>
     <td>   
     <input type="int" size="2" name="entrada" value="" id="10" onchange="multiplicar()">
     </td>
     <td>*4</td>
     <td>  
     <input type="int" size="2" name="entrada" value="" id="11" onchange="multiplicar()">
     </td>
     <td>*6</td>
     <td>
     <input type="int" size="2" name="entrada" value="" id="12">
     </td>
</tr>
<tr>
     <td align="center">Archivos</td>
     <td>
     <input type="int" size="2" name="entrada" value="" id="13" onchange="multiplicar()">
     </td>
     <td>*7</td>
     <td> 
     <input type="int" size="2" name="entrada" value="" id="14" onchange="multiplicar()">
     </td>
     <td>*10</td>
     <td>
     <input type="int" size="2" name="entrada" value="" id="15" onchange="multiplicar()">
     </td>
     <td>*15</td>
     <td>  
     <input type="int" size="2" name="entrada" value="" id="16">
     </td>
</tr>
<tr>
     <td align="center">Interfaces</td>
     <td>
     <input type="int" size="2" name="entrada" value="" id="17" onchange="multiplicar()">
     </td>
     <td>*5</td>
     <td> 
     <input type="int" size="2" name="entrada" value="" id="18" onchange="multiplicar()">
     </td>
     <td>*7</td>
     <td>
     <input type="int" size="2" name="entrada" value="" id="19" onchange="multiplicar()">
     </td>
     <td>*10</td>
     <td> 
     <input type="int" size="2" name="entrada" value="" id="20">
     </td>
</tr>
<tr>
     <td align="center" colspan="7">Total</td>
     <td> 
     <input type="int" size="2" name="entrada" value="" id="21">
     </td>
     </tr>
    </table>

    <table>
     <td nowrap>PFA = 
          <input type="int" size="2" name="entrada" value="" id="pfsa" onchange="multiplicar()">
          </td>
          <td>[0.65 + 0.01 * <input type="text" id="suma" onchange="multiplicar()"> ] => <input type="text" id="pfa"> </td>
    </table>


    <table>
     <p>KLOC = (<input type="text" id="multiplicar" onchange="multiplicar()">* <input type="text" id="codigo" onchange="multiplicar()">)/1000  => <input type="text" id="kloc">
     </p>
     <p>LOC=    <input type="text" id="operacion" onchange="multiplicar()">*1000  => <input type="text" id="loc">
     </p>
     </table>
<h1>BASICO</h1>
    <p><label for="name"> esfuerzo=<input type="text" id="esfuerzo"></label></p>
    <p><label for="name">tiempo=<input type="text" id="tiempo"></label></p>
    <p><label for="name">productividad=<input type="text" id="productividad"></label></p>
    <p><label for="name">personal=<input type="text" id="personal"></label></p>
<section id="intermedio1">

<h1>INTERMEDIO</h1>
<center><table  BORDER=5 CELLPADDING=10 CELLSPACING=10 bordercolor="white" <textarea style='font-size: 30px; font-weight: bold'></textarea>
<tr>
<td rowspan="2">CONDUCTORES DE COSTO</td>
    <td colspan="6" align="center">VALORACION</td>
</tr>
<tr>
     <td>Muy bajo</td>
     <td>bajo</td>
     <td>Nominal</td>
     <td>Alto</td>
     <td>Muy Alto</td>
     <td>Etra alto</td> 
</tr>
<tr>
<td>Fiabilidad requerida del software</td>
     <td>0,75<input type="checkbox" value="0.75" id="a1" onchange="intermedio()"></td>
     <td>0,88<input type="checkbox" value="0.88" id="a1" onchange="intermedio()"></td>
     <td>1.00<input type="checkbox" value="1" id="a1" onchange="intermedio()"></td>
     <td>1.15<input type="checkbox" value="1.15" id="a1" onchange="intermedio()"></td>
     <td>1.40<input type="checkbox" value="1.40" id="a1" onchange="intermedio()"></td>  
     <td>-</td>
</tr>
<tr>
     <td>Tamaño de la base de datos</td>
     <td>-</td>
     <td>0,94 <input type="checkbox" value="0.94" id="a1" onchange="intermedio()"></td>
     <td>1.00 <input type="checkbox" value="1" id="a1" onchange="intermedio()"></td>
     <td>1.08 <input type="checkbox" value="1.08" id="a1" onchange="intermedio()"></td>
     <td>1,16 <input type="checkbox" value="1.16" id="a1" onchange="intermedio()"></td>
     <td>-</td>
</tr>
<tr>
     <td>Complejidad del Producto</td>
     <td>0,70 <input type="checkbox" value="0.70" id="a1" onchange="intermedio()"></td>  
     <td>0,85 <input type="checkbox" value="0.85" id="a1" onchange="intermedio()"></td>
     <td>1.00 <input type="checkbox" value="1" id="a1" onchange="intermedio()"></td>   
     
     
     <td>1,15 <input type="checkbox" value="1.15" id="a1" onchange="intermedio()"></td>
     <td>1,30 <input type="checkbox" value="1.30" id="a1" onchange="intermedio()"></td>
     <td>1,65 <input type="checkbox" value="1.65" id="a1" onchange="intermedio()"></td>
     
</tr>
<tr>
     <td>Restricciones del tiempo de ejecucion</td>
     <td>
-
     </td>
     <td>-</td>
     <td>1.00<input type="checkbox" value="1" id="a1" onchange="intermedio()"> </td>
     <td>1,11<input type="checkbox" value="1.11" id="a1" onchange="intermedio()"></td>
     <td>1,30<input type="checkbox" value="1.30" id="a1" onchange="intermedio()"> </td>
     <td>1,66<input type="checkbox" value="1.66" id="a1" onchange="intermedio()"></td>
     
   
     
</tr>
<tr>
     <td>Restricciones del almacenamiento principal</td>
     <td>-</td>
     <td>-</td>
     <td> 1.00<input type="checkbox" value="1" id="a1" onchange="intermedio()"> </td>
     <td>1,06<input type="checkbox" value="1.06" id="a1" onchange="intermedio()"></td>
     <td> 1,21<input type="checkbox" value="1.21" id="a1" onchange="intermedio()"></td>
     <td>1,56 <input type="checkbox" value="1.56" id="a1" onchange="intermedio()"></td>
     
</tr>
<tr>
     <td>Volatilidad de la maquina virtual</td>
     <td> -
     </td>
     <td>0,87 <input type="checkbox" value="0.87" id="a1" onchange="intermedio()"></td>
     <td> 1.00<input type="checkbox" value="1" id="a1" onchange="intermedio()"></td>
     <td>1,15<input type="checkbox" value="1.15" id="a1" onchange="intermedio()"></td>
     <td>1,30<input type="checkbox" value="1.30" id="a1" onchange="intermedio()"> </td>
     <td>-</td>
     </tr>
     <tr>
     <td>Tiempo de Respuesta del ordenador</td>
     <td>- 
     </td>
     <td>0,87<input type="checkbox" value="0.87" id="a1" onchange="intermedio()"></td>
     <td> 1.00<input type="checkbox" value="1.00" id="a1" onchange="intermedio()">
     </td>
     <td>1,07<input type="checkbox" value="1.07" id="a1" onchange="intermedio()"></td>
     <td>1,15<input type="checkbox" value="1.15" id="a1" onchange="intermedio()">
     </td>
     <td>-</td>
     </tr>
     <tr>
     <td>Capacidad del analista</td>
     <td> 1,46<input type="checkbox" value="1.46" id="a1" onchange="intermedio()">
     </td>
     <td>1,19<input type="checkbox" value="1.19" id="a1" onchange="intermedio()"></td>
     <td>1.00<input type="checkbox" value="1.00" id="a1" onchange="intermedio()"> 
     </td>
     <td>0,86<input type="checkbox" value="0.86" id="a1" onchange="intermedio()"></td>
     <td> 0,71<input type="checkbox" value="0.71" id="a1" onchange="intermedio()">
     </td>
     <td>-</td>
     </tr>
     <tr>
     <td>Experiencia en la aplicacion</td>
     <td> 1,29<input type="checkbox" value="1.29" id="a1" onchange="intermedio()">
     </td>
     <td>1,13<input type="checkbox" value="1.13" id="a1" onchange="intermedio()"></td>
     <td>1.00<input type="checkbox" value="1.00" id="a1" onchange="intermedio()"></td>
     <td> 0,91<input type="checkbox" value="0.91" id="a1" onchange="intermedio()">
     </td>
     <td>0,82<input type="checkbox" value="0.82" id="a1" onchange="intermedio()"></td>
     <td> -
     </td>
     </tr>

     <tr>
     <td>Capacidad de los programadores</td>
     <td> 1,42<input type="checkbox" value="1.42" id="a1" onchange="intermedio()">
     </td>
     <td>1,17<input type="checkbox" value="1.17" id="a1" onchange="intermedio()"></td>
     <td> 1.00<input type="checkbox" value="1.00" id="a1" onchange="intermedio()">
     </td>
     <td>0,86<input type="checkbox" value="0,86" id="a1" onchange="intermedio()"></td>
     <td> 0,70<input type="checkbox" value="0.70" id="a1" onchange="intermedio()">
     </td>
     <td>-</td>
     
     </tr>
     <tr>
     <td>Experiencia de S.O. utilizado</td>
     <td> 1,21<input type="checkbox" value="1.21" id="a1" onchange="intermedio()">
     </td>
     <td>1,10<input type="checkbox" value="1.10" id="a1" onchange="intermedio()"></td>
     <td> 1.00<input type="checkbox" value="1.00" id="a1" onchange="intermedio()">
     </td>
     <td>0,90<input type="checkbox" value="0.90" id="a1" onchange="intermedio()"></td>
     <td> -
     </td>
     <td>-</td>
     
     </tr>
     <tr>
     <td>Experiencia en el lenguaje de programacion</td>
     <td> 1,14<input type="checkbox" value="1.14" id="a1" onchange="intermedio()">
     </td>
     <td>1,07<input type="checkbox" value="1.07" id="a1" onchange="intermedio()"></td>
     <td> 1,00<input type="checkbox" value="1.00" id="a1" onchange="intermedio()">
     </td>
     <td>0,95<input type="checkbox" value="0.95" id="a1" onchange="intermedio()"></td>
     <td> -
     </td>
     <td>-</td>
    
     </tr>
     <tr>
     <td>Practicas de programacion modernas</td>
     <td> 1,24<input type="checkbox" value="1.24" id="a1" onchange="intermedio()">
     </td>
     <td>1,10<input type="checkbox" value="1.10" id="a1" onchange="intermedio()"></td>
     <td> 1.00<input type="checkbox" value="1.00" id="a1" onchange="intermedio()">
     </td>
     <td>0,91<input type="checkbox" value="0.91" id="a1" onchange="intermedio()"></td>
     <td> 0,82<input type="checkbox" value="0.82" id="a1" onchange="intermedio()">
     </td>
     <td>-</td>
    
     </tr>
     <tr>
     <td>Utilizacion de herramientas software</td>
     <td> 1,24<input type="checkbox" value="1.24" id="a1" onchange="intermedio()">
     </td>
     <td>1,10<input type="checkbox" value="1.10" id="a1" onchange="intermedio()"></td>
     <td> 1.00<input type="checkbox" value="1.00" id="a1" onchange="intermedio()">
     </td>
     <td>0,91<input type="checkbox" value="0.91" id="a1" onchange="intermedio()"></td>
     <td> 0,83<input type="checkbox" value="0.83" id="a1" onchange="intermedio()">
     </td>
     <td>-</td>
     
     </tr>
     <tr>
     <td>Limitaciones de planificacion del proyecto</td>
     <td> 1,23<input type="checkbox" value="1.23" id="a1" onchange="intermedio()">
     </td>
     <td>1,08<input type="checkbox" value="1.08" id="a1" onchange="intermedio()"></td>
     <td> 1.00<input type="checkbox" value="1.00" id="a1" onchange="intermedio()">
     </td>
     <td>1,04<input type="checkbox" value="1.04" id="a1" onchange="intermedio()"></td>
     <td> 1,10<input type="checkbox" value="1.10" id="a1" onchange="intermedio()">
     </td>
     <td>-</td>    
     </tr>
    </table>
    <label for="fa">FA</label>
    <input type="number" id="fa">
    <p><label for="name"> esfuerzo=<input type="text" id="e"></label></p>
    <p><label for="name">tiempo=<input type="text" id="t"></label></p>
    <p><label for="name">productividad=<input type="text" id="pro"></label></p>
    <p><label for="name">personal=<input type="text" id="per"></label></p>
    </section>    
<h1>AVANZADO</h1>
</body>
</html>